<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Class TeamPresenter
 * @package App\Presenters
 */
class TeamPresenter extends BasePresenter
{
    /** @var \App\Components\IAddEditTeamFormFactory @inject */
    public $addEditTeamForm;

    /** @var \App\Components\IUpdatePasswordFormFactory @inject */
    public $updatePasswordForm;

    protected function createComponentTeamEditForm() {
        $component = $this->addEditTeamForm->create();
        return $component->setId($this->user->getId());
    }

    protected function createComponentUpdatePasswordForm() {
        return $this->updatePasswordForm->create();
    }
}
