<?php
/**
 * Sign in form component
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Components;

use Nette\Application\UI;

/**
 * Class SignInForm
 * @package App\Components
 */
class SignInForm extends UI\Control
{
    use TemplateTrait;

    protected function createComponentForm() {
        $form = new UI\Form();

        $form->setTranslator($this->presenter->translator);

        $form->addText('email', 'components.signInForm.email.title')
            ->setRequired('components.signInForm.email.required')
            ->addRule(UI\Form::EMAIL, 'components.signInForm.email.valid');

        $form->addPassword('password', 'components.signInForm.password.title')
            ->setRequired('components.signInForm.password.required');

        $form->addSubmit('submit', 'components.signInForm.submit');

        $form->onSuccess[] = $this->processForm;

        return $form;
    }

    public function processForm(UI\Form $form) {
        $values = $form->getValues();

        $this->presenter->getUser()->setExpiration('14 days', TRUE);

        try {
            $this->presenter->getUser()->login($values->email, $values->password);
            $this->flashMessage('components.signInForm.flashes.success', 'success');
            if(isset($this->presenter->backlink)) {
                $this->presenter->restoreRequest($this->presenter->backlink);
            }
            $this->presenter->redirect(':Team:settings');

        } catch (\Nette\Security\AuthenticationException $e) {
            $form->addError($this->presenter->translator->translate($e->getMessage()));
        }
    }
}

/**
 * Interface ISignInFormFactory
 * @package App\Components
 */
interface ISignInFormFactory
{
    /**
     * Creates sign in form component
     * @return \App\Components\SignInForm
     */
    function create();
}
